# Entwurf des Gründungsprogramms der Klimaliste Deutschland

Hier findet ihr die Dateien, die ihr für die Einreichung der Änderungsanträge durch die Landesverbände benötigt.

- Unter "Repository" -> "Branches" findet ihr die bereits eingepflegten Anträge, die per Email eingingen. Mit einem Klick auf "Compare" beim jeweiligen Branch könnt ihr prüfen, was geändert wurde.
- Bitte belasst beim Bearbeiten die Zeilenumbruch-Regeln, das heißt: Jeder Satz in eine neue Zeile, ein Absatz wird durch eine Leerzeile symbolisiert.
- Redaktionelle Änderungen bitte nicht als Antrag einreichen, das macht nur Ärger beim Einpflegen.
- Das Repository kann lokal ausgecheckt werden; das ist vor allem für die Moderation der Themencalls wichtig. Mit dem Befehl

 ```git log --all --source -- Energie.txt  | grep -o "refs/.*" | sort -u``` 
 
lassen sich dann alle Branches anzeigen, die die Datei `Energie.txt` verändert haben.