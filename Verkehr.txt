Wir streben eine Verkehrswende an, die die Abhängigkeit vom motorisierten Individualverkehr überwindet. 
Unser städtebauliches Leitbild ist die „Stadt der kurzen Wege“, in der Wohnen, Einkaufen, Arbeiten und Freizeitaktivitäten in räumlicher Nähe angeboten werden. 
Dabei sollen klimaschonender Individualverkehr und ÖPNV bevorzugt und gefördert werden. 
Im ländlichen Raum sollen Bedigungen für einen sozialen und klimafreundlichen Verkehr geschaffen werden.

Der Personen- und Güterfernverkehr soll im Wesentlich auf der Schiene stattfinden. 
Die dafür notwendigen Kapazitäten müssen so schnell wie möglich aufgebaut werden.

Für den Güterverkehr werden klimafreundliche Rahmenbedingen geschaffen.
Grundsätzlich muss die Bevorzugung von LKW und PKW in der Verkehrsplanung beendet werden.